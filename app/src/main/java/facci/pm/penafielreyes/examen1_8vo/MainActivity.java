package facci.pm.penafielreyes.examen1_8vo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String titulo;

    RequestQueue queque;
    String Url = "https://jsonplaceholder.typicode.com/albums";

    List<String> datos = datos = new ArrayList<String>();
    ListView listdatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queque = Volley.newRequestQueue(this);

        GetApiData();
        listdatos = (ListView)findViewById(R.id.list_books);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,datos);
        //listdatos.setAdapter(adapter);
    }


    private void GetApiData(){
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response.length() > 0){
                    for (int i=0 ; i < response.length(); i ++){
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Albums albums = new Albums();
                            albums.setId(Integer.parseInt(obj.get("id").toString()));
                            albums.setUserId(Integer.parseInt(obj.get("userId").toString()));
                            albums.setTitle(obj.get("title").toString());

                            datos.add(obj.get("title").toString());

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item,datos);
                            listdatos.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }



        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int x = 0;
            }
        });

        queque.add(request);

    }

}