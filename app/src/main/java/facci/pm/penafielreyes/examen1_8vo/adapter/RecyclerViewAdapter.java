package facci.pm.penafielreyes.examen1_8vo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import facci.pm.penafielreyes.examen1_8vo.Albums;
import facci.pm.penafielreyes.examen1_8vo.DetailActivity;
import facci.pm.penafielreyes.examen1_8vo.R;

public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Albums> mData;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.activity_detail, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, DetailActivity.class);
                i.putExtra("albun_title", mData.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("albun_id", mData.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("albun_idUser", mData.get(viewHolder.getAdapterPosition()).getUserId());


                mContext.startActivity(i);

            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.al_title.setText(mData.get(position).getTitle());
        holder.al_user.setText(mData.get(position).getUserId());
        holder.al_id.setText(mData.get(position).getId());


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView al_title;
        TextView al_user;
        TextView al_id;

        public MyViewHolder(View itemView) {
            super(itemView);

            //al_title = itemView.findViewById(R.id.container);
            //al_user = itemView.findViewById(R.id.anime_name);
            //al_id = itemView.findViewById(R.id.categorie);

        }

    }
}

